use mavinspect::parser::Inspector;
use mavinspect::protocol::metadata::DialectTags;

fn all_dialect_paths() -> Vec<&'static str> {
    vec![
        "./message_definitions/standard",
        "./message_definitions/extra",
        "./message_definitions/extra/inheritance",
    ]
}

#[test]
fn test_dialect_metadata() {
    let inspector = Inspector::builder()
        .set_sources(&all_dialect_paths())
        .build()
        .unwrap();
    let protocol = inspector.parse().unwrap();

    let mav_inspect_test = protocol
        .get_dialect_by_canonical_name("mav_inspect_test")
        .unwrap();

    assert!(mav_inspect_test.metadata().has_tag("extra"));
    assert!(mav_inspect_test.metadata().has_tag("test"));
    assert!(!mav_inspect_test.metadata().has_tag("crazy"));

    let crazy_flight = protocol
        .get_dialect_by_canonical_name("crazy_flight")
        .unwrap();

    assert!(crazy_flight.metadata().has_tag("extra"));
    assert!(crazy_flight.metadata().has_tag("test"));
    assert!(crazy_flight.metadata().has_tag("crazy"));

    let ext_a = protocol.get_dialect_by_canonical_name("ext_a").unwrap();

    assert!(ext_a.metadata().has_tag("inheritance"));
    assert!(ext_a.metadata().has_tag("test"));

    let standard_dialect = protocol.get_dialect_by_canonical_name("common").unwrap();

    assert!(standard_dialect.metadata().tags().is_empty());
}

#[test]
fn test_dialects_filtering_by_name_and_tags() {
    let inspector = Inspector::builder()
        .set_sources(&all_dialect_paths())
        .build()
        .unwrap();
    let protocol = inspector.parse().unwrap();

    let protocol =
        protocol.with_dialects_included(&["common"], true, Some(DialectTags::new(&["test"])));

    // Should have these since names were included
    assert!(protocol.contains_dialect_with_canonical_name("common"));
    assert!(protocol.contains_dialect_with_canonical_name("standard"));
    assert!(protocol.contains_dialect_with_canonical_name("minimal"));
    // Should not have these
    assert!(!protocol.contains_dialect_with_canonical_name("development"));
    assert!(!protocol.contains_dialect_with_canonical_name("all"));

    // Should have these since tags were included
    assert!(protocol.contains_dialect_with_canonical_name("crazy_flight"));
    assert!(protocol.contains_dialect_with_canonical_name("mav_inspect_test"));
}
