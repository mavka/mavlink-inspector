use mavinspect::parser::Inspector;

fn main() {
    // Setup logger
    env_logger::builder()
        // Suppress everything below `info` for third-party modules.
        .filter_level(log::LevelFilter::Info)
        // Allow everything from current package
        .filter_module(env!("CARGO_PKG_NAME"), log::LevelFilter::Trace)
        .init();

    // Instantiate inspector and load list of XML definitions
    let inspector = Inspector::builder()
        .set_sources(&[
            // Standard definitions from
            // https://github.com/mavlink/mavlink/tree/master/message_definitions/v1.0
            "./message_definitions/standard",
            // Extra definitions which depend on standard dialects
            "./message_definitions/extra",
        ])
        .set_include(&[
            "ardupilotmega",
            "common",
            "matrixpilot",
            // Custom dialects
            "CrazyFlight",
            "MAVInspect_test",
        ])
        .set_exclude(&vec!["matrixpilot", "paparazzi"])
        .build()
        .unwrap();

    // Parse all XML definitions
    let protocol = inspector.parse().unwrap();

    // Check that interesting dialects were included into final output
    assert!(protocol.contains_dialect_with_canonical_name("mav_inspect_test"));
    assert!(protocol.contains_dialect_with_canonical_name("crazy_flight"));
    assert!(protocol.contains_dialect_with_canonical_name("common"));

    // Get `common` dialect from standard definitions
    let common = protocol.get_dialect_by_canonical_name("common").unwrap();

    // Inspect `MAV_CMD_NAV_WAYPOINT` command of the `common` dialect
    let command = common.get_command_by_name("MAV_CMD_NAV_WAYPOINT").unwrap();
    assert!(command.has_location());
    assert!(command.is_destination());
    assert!(!command.mission_only());
    log::warn!("\n`MAV_CMD_NAV_WAYPOINT` command (`minimal` dialect): {command:#?}",);

    // Inspect `MAV_CMD_NAV_ROI` command of the `common` dialect
    let command = common.get_command_by_name("MAV_CMD_NAV_ROI").unwrap();
    assert!(command.has_location());
    assert!(!command.is_destination());
    assert!(!command.mission_only());
    let first_param_enum_name = command.params().first().unwrap().r#enum().unwrap();
    assert_eq!(first_param_enum_name, "MAV_ROI");
    log::warn!("\n`MAV_CMD_NAV_ROI` command (`minimal` dialect): {command:#?}",);

    // Inspect `MAV_ROI` enum of the `common` dialect
    let mav_enum = common.get_enum_by_name(first_param_enum_name).unwrap();
    assert_eq!(mav_enum.defined_in(), "common");
    log::warn!("\n`MAV_ROI` enum (`minimal` dialect): {mav_enum:#?}",);

    // Get `crazyflight` custom-defined dialect
    let crazyflight = protocol
        .get_dialect_by_canonical_name("crazy_flight")
        .unwrap();

    // Inspect `DUMMYFLIGHT_OUTCRY` message
    let outcry_message = crazyflight.get_message_by_id(54000u32).unwrap();
    assert_eq!(outcry_message.name(), "CRAZYFLIGHT_OUTCRY");
    log::warn!("\n`CRAZYFLIGHT_OUTCRY` message: {outcry_message:#?}");

    // Inspect `HEARTBEAT` message which custom dialect inherits from `standard` dialect
    let heartbeat_message = crazyflight.get_message_by_id(0u32).unwrap();
    assert_eq!(heartbeat_message.name(), "HEARTBEAT");
    // Verify that `HEARTBEAT` message is defined in `minimal` dialect
    assert_eq!(heartbeat_message.defined_in(), "minimal");
}

#[cfg(test)]
#[test]
fn test_parser_example() {
    main()
}
