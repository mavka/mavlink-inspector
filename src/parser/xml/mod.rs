mod context;

mod context_stack;

mod parser;
pub use parser::XmlParser;

pub mod entities;
